Source: xfce4-goodies
Section: metapackages
Priority: optional
Maintainer: Debian Xfce Maintainers <debian-xfce@lists.debian.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.5.0
Homepage: https://goodies.xfce.org/
Vcs-Git: https://salsa.debian.org/xfce-team/goodies/xfce4-goodies.git
Vcs-Browser: https://salsa.debian.org/xfce-team/goodies/xfce4-goodies

Package: xfce4-goodies
Architecture: any
Depends: mousepad,
         ristretto,
         thunar-archive-plugin,
         thunar-media-tags-plugin,
         xfburn,
         xfce4-battery-plugin [linux-any kfreebsd-i386],
         xfce4-clipman-plugin,
         xfce4-cpufreq-plugin [linux-any],
         xfce4-cpugraph-plugin [linux-any],
         xfce4-datetime-plugin,
         xfce4-dict,
         xfce4-diskperf-plugin [linux-any],
         xfce4-fsguard-plugin,
         xfce4-genmon-plugin,
         xfce4-mailwatch-plugin,
         xfce4-netload-plugin [linux-any],
         xfce4-notifyd | notification-daemon | notify-osd,
         xfce4-places-plugin,
         xfce4-screenshooter,
         xfce4-sensors-plugin [linux-any kfreebsd-any],
         xfce4-smartbookmark-plugin,
         xfce4-systemload-plugin,
         xfce4-taskmanager,
         xfce4-terminal,
         xfce4-timer-plugin,
         xfce4-verve-plugin,
         xfce4-wavelan-plugin,
         xfce4-weather-plugin,
         xfce4-whiskermenu-plugin,
         xfce4-xkb-plugin,
         ${misc:Depends}
Recommends: xfce4-power-manager
Suggests: gigolo,
          parole,
          xfce4-indicator-plugin,
          xfce4-mpc-plugin,
          xfce4-notes-plugin,
          xfce4-radio-plugin [linux-any],
Description: enhancements for the Xfce4 Desktop Environment
 The "Goodies for Xfce" project includes additional software and artwork that
 are related to the Xfce desktop, but not part of the official release.
 .
 This package will install the following Xfce4 related plugins:
   * Extra artwork (xfce4-artwork)
   * Battery levels monitor (xfce4-battery-plugin)
   * Clipboard history (xfce4-clipman-plugin)
   * CPU frequency management plugin (xfce4-cpufreq-plugin)
   * CPU utilisation graphs (xfce4-cpugraph-plugin)
   * Date and time plugin (xfce4-datetime-plugin)
   * Disk performance display (xfce4-diskperf-plugin)
   * Filesystem monitor (xfce4-fsguard-plugin)
   * Generic monitor, for displaying any command result (xfce4-genmon-plugin)
   * Mail watcher (xfce4-mailwatch-plugin)
   * Network load monitor (xfce4-netload-plugin)
   * Notes plugin (xfce4-notes-plugin)
   * Quick access to bookmarked folders, recent documents and removable
     media (xfce4-places-plugin)
   * Sensors plugin, frontend to lm-sensors (xfce4-sensors-plugin)
   * Smartbookmarks plugin (xfce4-smartbookmark-plugin)
   * System load monitor (xfce4-systemload-plugin)
   * Timer plugin (xfce4-timer-plugin)
   * Command line with history (xfce4-verve-plugin)
   * Wireless lan monitor (xfce4-wavelan-plugin)
   * Weather monitor (xfce4-weather-plugin)
   * Keyboard configuration (xfce4-xkb-plugin)
   * Archive management for Thunar (thunar-archive-plugin)
   * Media tags editor for Thunar (thunar-media-tags-plugin)
   * Alternate menu plugin (xfce4-whiskermenu-plugin)
 .
 It'll install some standalone applications too:
   * Tiny text editor (mousepad)
   * Images viewer (ristretto)
   * CD/DVD burner (xfburn)
   * Frontend to dictionaries (xfce4-dict)
   * Notification daemon (xfce4-notifyd)
   * Tool to take screenshots (xfce4-screenshooter)
   * Task manager (xfce4-taskmanager)
   * Terminal emulator (xfce4-terminal)
 .
 Some packages are only suggested because they bring too much dependencies,
 but you may find them interesting:
   * Cellular modem plugin (xfce4-cellmodem-plugin)
   * Indicator (conform to the Ubuntu MessagingMenu specification) plugin
     (xfce4-indicator-plugin)
   * Another commandline plugin (xfce4-minicmd-plugin)
   * Frontends to MPD (xfce4-mpc-plugin, xfmpc)
   * Radio plugin (xfce4-radio-plugin))
   * GIO/GVfs frontend to manage connections to remote filesystems (gigolo)
   * Media player (parole)
   * Power Manager (xfce4-power-manager)
 .
 This is a metapackage to ease upgrades, installations, and provide a
 consistent upgrade path from previous versions. It can safely be removed with
 no ill effects.
